#!/bin/bash
#.bashrc

set -o vi

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
#===============================================================
# GENERIC ENVIRONMENT VARIABLES
#===============================================================


alias httpd.c='ssubl /etc/httpd/conf/httpd.conf'
alias ssl.c='ssubl /etc/httpd/d.conf/ssl.conf'

PS1="\[\033[0;37m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[0;31m\]\h'; else echo '\[\033[0;33m\]\u\[\033[0;37m\]@\[\033[0;96m\]\h'; fi)\[\033[0;37m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;37m\]]\n\[\033[0;37m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]"
alias .bze='cat /dev/urandom | hexdump -C | grep "ca fe"'
alias ssync="sh ./sync"
export HISTCONTROL=ignoreboth:erasedups
export bashrc=~/.bashrc
shopt -s extglob
alias ddate='date +"%d-%b-%Y-%H.%M.%S"'
alias chmod.*.644='find . -type f -print0 | xargs -0 chmod 644'
alias chmod.*.755='find . -type d -print0 | xargs -0 chmod 755'
alias chowngrp='chown '$*' && chgrp '$*
alias chx='chmod +x'
alias brc='cat '$bashrc' | grep '$@
alias sub='sudo subl'$@
alias jj2.db='mysql -h jj2db.ciw3g743zryw.us-east-1.rds.amazonaws.com -u ourweakness -pf1reWater_d'
alias tarc='tar cvzf'
alias ssubl='sudo subl'
alias trc='cat $bashrc | grep "$@"'
alias tdate='date +%m-%d-%y.%R'

	function killall() {
		for i in `ps aux | grep $@ | grep -v grep | awk {'print $2'}` ; do kill $i; done
	}
	function echo.r() { echo "\$ $@" ; echo `$@` ; }


	function ser() {
		echo.r "sudo service "$@" restart"
	}

	function trc(){
		if [[ $# != 0 ]]; then
			echo `cat $bashrc | grep "$@"`
		else
			echo 'no arguments'
		fi
	}
	# ~ VARIABLES
	#===============================

	export pg=~/pg/
	export dev=~/dev/
	export git=~/dev/git/

	function bak(){
		NOW=$(tdate)
		echo.r cp -rf $1 $1.$NOW.bak
	}
#===============================================================
# COMMON COMMAND ALIASES
#===============================================================

	# The 'ls' family (this assumes you use a recent GNU ls)
	#===============================================================

	alias ll='ls -l --group-directories-first'
		alias la='ls -Al'          # show hidden files
		alias lx='ls -lXB'         # sort by extension
		alias lk='ls -lSr'         # sort by size, biggest last
		alias lc='ls -ltcr'        # sort by and show change time, most recent last
		alias lu='ls -ltur'        # sort by and show access time, most recent last
		alias lt='ls -ltr'         # sort by date, most recent last
		alias lm='ls -al |more'    # pipe through 'more'
		alias lr='ls -lR'          # recursive ls
		alias ..='cd ..'

	# 'rm'
	#===============================

	alias rm='rm -i'
	alias cp='cp -i'
	alias mv='mv -i'

	# 'cd'
	#===============================

	alias ..='cd ..'
	alias ...='cd ../../../'
	alias ....='cd ../../../../'
	alias .....='cd ../../../../'

	# git aliases
	#===============================
	alias ga='git add'
	alias gp='git push'
	alias gl='git log'
	alias gs='git status'
	alias gd='git diff'
	alias gdc='git diff --cached'
	alias gm='git commit -m'
	alias gma='git commit -am'
	alias gb='git branch'
	alias gc='git checkout'
	alias gra='git remote add'
	alias grr='git remote rm'
	alias gpu='git pull'
	alias gcl='git clone'
#===============================================================
# FUNCTIONS
#===============================================================

	# universal extractor
	#===============================

	extract () {
		if [ -f $1 ] ; then
			case $1 in
				*.tar.bz2)   tar xvjf $1    ;;
*.tar.gz)    tar xvzf $1    ;;
*.bz2)       bunzip2 $1     ;;
*.rar)       unrar x $1       ;;
*.gz)        gunzip $1      ;;
*.tar)       tar xvf $1     ;;
*.tbz2)      tar xvjf $1    ;;
*.tgz)       tar xvzf $1    ;;
*.zip)       unzip $1       ;;
*.Z)         uncompress $1  ;;
*.7z)        7z x $1        ;;
*)           echo "don't know how to extract '$1'..." ;;
esac
else
	echo "'$1' is not a valid file!"
fi
}

#===============================================================
# OS SPECIFIC VARIABLES
#===============================================================

	OS="`uname`"
	case $OS in
	  'Linux')
	    OS='Linux'
	    alias ls='ls -hF --color --group-directories-first'
	    alias subl='sudo rsub'
	    export EDITOR='vim'
		    # cd function changed to ls directory after cd.
			#===============================

			function cd() {
				builtin cd "$@" && ls
			}
		export PATH=$PATH:~/bin
	    ;;
	  'WindowsNT')
	    OS='Windows'
	    ;;
	  'Darwin')
	    OS='Mac'
	    # export JAVA_HOME="$(/usr/libexec/java_home)"
	    export PATH=$PATH:/usr/local/sbin:/usr/local/pear/bin:~/bin
	    export AWS_ACCESS_KEY='AKIAJAM37H3UISLG3Z2A'
		export AWS_SECRET_KEY='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'
		export EC2_URL='https://ec2.us-west-2.amazonaws.com'
		export PATH=$PATH:/usr/local/sbin:/usr/local/pear/bin:~/bin:/usr/local/Cellar/php55/5.5.8/bin/
    	export HOMEBREW_GITHUB_API_TOKEN='6375536473c6f8f914d88596ce88bae1d5c5b85b'
    	export EDITOR='subl'

		logout() {
		  osascript -e 'tell application "System Events" to log out'
		  builtin logout
		}
		alias ls='gls -hFGp --color=always --group-directories-first'  # add colors for filetype recognition
		alias srn='sudo shutdown -r now'
			# cd function changed to ls directory after cd.
			#===============================

			function cd() {
				builtin cd "$@" && gls -hFGp --color=always --group-directories-first
			}
	    ;;
	esac